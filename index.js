function autoComment() {
    const comments = [
        '~ Xinh quá!',
        '~ Duyn ơi cố lên!',
        '~ Mọi người đi ngang qua cho con bé xin một follow ạ! || Follow please, thank you!',
        '~ Cám ơn mọi người đã quan tâm idol nhạt nhẽo này ạ!'
    ];
    var input = document.getElementsByClassName("nimo-room__chatroom__chat-box__input")[0];
    var submit = document.getElementsByClassName('nimo-chat-box__send-btn')[0];

    var comment = comments[Math.floor(Math.random() * comments.length)];
    console.log(new Date().toISOString() + ' - ' + comment);
    input.value = comment;
    submit.click();
}

autoComment();
setInterval(() => {
    autoComment();
}, 10 * 60 * 1000);